package com.example.demo;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/{num}")
	public String index(@PathVariable int num, Model model) {
		int res = 0;
		for(int i =1; i < num;i++) {
			res +=i;
		}
		model.addAttribute("msg","total" + res);
		return "index";
		//return "Hello worldaab" + Integer.toString(num);
	
	}
}
